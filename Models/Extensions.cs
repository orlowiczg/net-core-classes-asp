using System.Collections.Generic;

namespace asp.Models
{
    public static class Extensions
    {
        public static void Seed(this AppDbContext ctx)
        {
            List<Employee> emps = new List<Employee>()
            {
                new Employee(){
                    Name = "Jan",
                    Surname = "Kowalski",
                    Position = "Asystent"
                },
                new Employee()
                {
                    Name = "Paweł",
                    Surname = "Nowak",
                    Position = "Adiunkt"
                },
                new Employee()
                {
                    Name = "Paweł",
                    Surname = "Janicki",
                    Position = "Profesor zwyczajny"
                }
            };
            ctx.AddRange(emps);
            ctx.SaveChanges();
        }
        
    }
}