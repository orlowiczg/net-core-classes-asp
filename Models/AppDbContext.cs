using Microsoft.EntityFrameworkCore;

namespace asp.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }
        public DbSet<Employee> emps {get;set;}

    }
}