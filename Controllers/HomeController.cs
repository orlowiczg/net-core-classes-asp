﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using asp.Models;

namespace asp.Controllers
{
    public class HomeController : Controller
    {
        private AppDbContext _ctx;
        public HomeController(AppDbContext ctx )
        {
            _ctx = ctx;
        }

        public IActionResult Index()
        {
            var result = _ctx.emps.OrderBy(c => c.Id).ToList();

            return View(result);
        }

        [HttpGet]
        public IActionResult Create()
        {
                return View();
        }

        [HttpPost]
        public IActionResult Create(Employee emp)
        {
            if(ModelState.IsValid)
            {
                _ctx.emps.Add(emp);
                _ctx.SaveChanges();

            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            Employee emp = _ctx.emps.FirstOrDefault(e => e.Id == id);
            ViewData["Message"] = "Your application description page.";
            return View(emp);
        }

        [HttpPost]
        public IActionResult Edit(Employee emp)
        {
            if(ModelState.IsValid)
            {
                _ctx.emps.Update(emp);
                _ctx.SaveChanges();

            }
            return RedirectToAction("Index");
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
